<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminLTEController;
use App\Http\Controllers\AdminLTEStudentController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('student/create', 'StudentController@create')
    ->name('student.create')->middleware('login_auth');

Route::post('/student', 'StudentController@store')
    ->name('student.store')->middleware('login_auth');

Route::get('/student', 'StudentController@index')
    ->name('student.index')->middleware('login_auth');

Route::get('/student/{student}', 'StudentController@show')
    ->name('student.show')->middleware('login_auth');

Route::get('/student/{student}/edit', 'StudentController@edit')
    ->name('student.edit')->middleware('login_auth');

Route::patch('/student/{student}', 'StudentController@update')
    ->name('student.update')->middleware('login_auth');

Route::delete('/student/{student}', 'StudentController@destroy')
    ->name('student.destroy')->middleware('login_auth');

Route::get('/login', 'AdminController@index')->name('login.index');

Route::get('/logout', 'AdminController@logout')->name('login.logout');

Route::post('/login', 'AdminController@process')->name('login.process');

Route::get('/adminlte/index', [AdminLTEController::class, 'index'])
    ->name('adminlte.index')->middleware('login_auth');

Route::post('/adminlte/student', [AdminLTEStudentController::class, 'store'])
    ->name('adminlte.student.store')->middleware('login_auth');

Route::get('/adminlte/student', [AdminLTEStudentController::class, 'index'])
    ->name('adminlte.student.index')->middleware('login_auth');

Route::get('/adminlte/student/create', [AdminLTEStudentController::class, 'create'])
    ->name('adminlte.student.create')->middleware('login_auth');

Route::get('/adminlte/student/show/{student}', [AdminLTEStudentController::class, 'show'])
    ->name('adminlte.student.show')->middleware('login_auth');

Route::get('/adminlte/student/edit/{student}', [AdminLTEStudentController::class, 'edit'])
    ->name('adminlte.student.edit')->middleware('login_auth');

Route::patch('/adminlte/student/update/{student}', [AdminLTEStudentController::class, 'update'])
    ->name('adminlte.student.update')->middleware('login_auth');

Route::delete('/adminlte/student/destroy/{student}', [AdminLTEStudentController::class, 'destroy'])
    ->name('adminlte.student.destroy')->middleware('login_auth');